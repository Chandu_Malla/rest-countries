import React from 'react'
import { createRoot } from 'react-dom/client';
import RouterComponent from './components/RouterComponent/RouterComponent.jsx';
import './index.css'
import { HeaderProvider } from './context/HeaderContext.jsx';
import { ChakraProvider } from '@chakra-ui/react';

createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <HeaderProvider>
      <ChakraProvider>
      <RouterComponent />
      </ChakraProvider>
    </HeaderProvider>
  </React.StrictMode>,
)
