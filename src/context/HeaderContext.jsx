import React, { createContext } from "react";
import { useContext, useState } from "react";

const HeaderContext = React.createContext()

export const useHeaderContext =()=>{
    return useContext(HeaderContext)
}

export function HeaderProvider({children}){

    const [darkModeValue, setDarkModeValue] = useState(false)

    const value= {
        darkModeValue,
        setDarkModeValue
    }
    return <HeaderContext.Provider value={value}>{children}</HeaderContext.Provider>
}