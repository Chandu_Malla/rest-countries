import { createContext, useEffect, useState } from 'react'
import LoadingSpinner from './components/LoadingSpinner/LoadingSpinner'
import { useHeaderContext } from './context/HeaderContext'
import DarkModeButton from './components/DarkModeButton/DarkModeButton'
import HeaderComponent from './components/HeaderComponent/HeaderComponent'
import FilterByRegionOptions from './components/FilterByRegionOptions/FilterByRegionOptions'
import SearchCountries from './components/SearchCountries/SearchCountries'
import ShowFlags from './components/ShowFlags/ShowFlags'
import FilterBySubRegion from './components/FilterBySubRegion/FilterBySubRegion'
import Population from './components/Population/Population'
import SortByArea from './components/SortByArea/SortByArea'
import CountryDetail from './components/CountryDetail/CountryDetail'
import { Flex, Box } from '@chakra-ui/react'
// import './App.css'


// const ContextHook = createContext()

const App = () =>  {

  const API_URL = 'https://restcountries.com/v3.1/all'

  const [loading, setLoading] = useState(true)

  const [backgroundStyle, setBackgroundStyle] = useState({ backgroundColor: 'white' })
  //  const [darkModeValue, setDarkModeValue] = useState(false)
  const {darkModeValue, setDarkModeValue} = useHeaderContext()


  const [allCountries, setAllCountries] = useState(null)

      useEffect(() => {
        
        const fetchCountries = async() => {
            try {
              const response = await fetch(API_URL)
              
              if (!response.ok) {
                throw new Error('Failed to fetch data')
              }
              const jsonData =  await response.json()

              setAllCountries(jsonData)
              setFlagsData(jsonData)

            } catch (error) {
              console.error('Error fetching data', error)
            } finally {
              setLoading(false)
            }
        }

        fetchCountries()
      }, [])

      
  // Update darkModeValue
      useEffect(() => {
        setBackgroundStyle({
          backgroundColor: darkModeValue ? 'black' : 'white'
        })


      }, [darkModeValue])
  

  
  // for input search
  const [searchCountries, setSearchCountries] = useState(null)
  const [searchText, setSearchText] = useState(null)

  /// for filter countries
  const [filteredCountries, setFilteredCountries] = useState(null)
  const [optionValue, setOptionValue] = useState(null)

  // for both functions
    const [bothInputs, setBothInputs] = useState(null)

  // for flagsData
  const [flagsData, setFlagsData] = useState(null)


  const updateSearchCountries = (data) => {
    setSearchCountries(data)
    setFlagsData(data)
  }


  const updateSearchText = (data) => {
    setSearchText(data)
  }

  const updateFilteredCountries = (data) =>  {
    setFilteredCountries(data)
    setFlagsData(data)
  }


  const updateOption = (data) => {
    setOptionValue(data)
  }

  // for both inputs 
  const updateBothInputs = (data) => {
      setBothInputs(data)
      setFlagsData(data)
  }



  useEffect(() => {
    if (searchText && optionValue ){ 
      const matchedCountries = filteredCountries.filter((country) => {
        const inputString = new RegExp(searchText.trim(), 'i')
        return (inputString.test(country.name.common) && country.region === optionValue)
    })

    updateBothInputs(matchedCountries)

    }
  }, [searchText, optionValue])


  // Creating subRegions Map
  const subRegionsMap = new Map()

  const collectSubRegions = () => {
    if(!allCountries) return null
    allCountries.forEach((country) => {
        if ( !subRegionsMap.has(country.region) )  {
          subRegionsMap.set(country.region, new Set())
        } else {
          subRegionsMap.get(country.region).add(country.subregion)
        }
    })
  }

  collectSubRegions()

// for subregions

const [subRegionData, setSubRegionData] = useState(null)

const updateSubRegionData = (data) => {
  setSubRegionData(data)
  setFlagsData(data)
}



// for populationData
const [populationData, setPopulationData] = useState(null)

const updatePopulationData = (data) => {
  setPopulationData(data)
  setFlagsData(data)
}


// for Area Data
const [areaData, setAreaData] = useState(null)

const updateAreaData = (data) => {
  setAreaData(data)
  setFlagsData(data)
}

return (
  <Box maxWidth='100vw'  >
    {loading ? (
      <LoadingSpinner /> 
    ) : (
      <Box  style={backgroundStyle}>
        <HeaderComponent />

        <Flex
          alignItems="center"
          justifyContent="space-between"
          m="2rem"
          spacing={4}
          style={{ color: !darkModeValue ? 'hsl(209, 23%, 22%)' : 'hsl(0, 0%, 98%)' }}
        >
          <Box w="25%">
            <SearchCountries
              allCountries={allCountries}
              updateSearchCountries={updateSearchCountries}
              updateSearchText={updateSearchText}
            />
          </Box>
          <Box>
            <FilterByRegionOptions
              allCountries={allCountries}
              updateFilteredCountries={updateFilteredCountries}
              updateOption={updateOption}
            />
          </Box>
          {optionValue && (
            <Box>
              <FilterBySubRegion
                allCountries={allCountries}
                subRegionsMap={subRegionsMap}
                optionValue={optionValue}
                updateSubRegionData={updateSubRegionData}
              />
            </Box>
          )}
          {allCountries && (
            <Box>
              <Population allCountries={allCountries} updatePopulationData={updatePopulationData} />
            </Box>
          )}
          {allCountries && (
            <Box>
              <SortByArea allCountries={allCountries} updateAreaData={updatePopulationData} />
            </Box>
          )}
        </Flex>

        <Box w='100vw' minHeight='100vh'>
          {!flagsData ? <LoadingSpinner/> : <ShowFlags countriesData={flagsData} darkModeValue={darkModeValue} />}
        </Box>
      </Box>
    )}
  </Box>
);

}
export default App