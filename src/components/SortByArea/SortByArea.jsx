// import './SortByArea.css'
import { Select, VStack } from '@chakra-ui/react';

const SortByArea = ({allCountries, updateAreaData}) => {
    const countryArea = allCountries.slice()

    const sortAscending = () => {
        countryArea.sort((countryA, countryB) =>  {
            return countryA.area - countryB.area
        })

        updateAreaData(countryArea)
    }


    const sortDescending = () => {
        countryArea.sort((countryA, countryB) =>  {
            return countryB.area - countryA.area
        })

        updateAreaData(countryArea)
    }



    const handleArea = (selecteareaOrder) =>  {
        // const selecteareaOrder = event.target.value

        if (selecteareaOrder === "Ascending"){
            sortAscending()
        } else {
            sortDescending()
        }

    }

    return (
        <VStack spacing={4}>
          <Select id="selectArea" onChange={(e) => handleArea(e.target.value)}>
            <option value="">Choose Area Order</option>
            <option value="Ascending">Ascending Order</option>
            <option value="Descending">Descending Order</option>
          </Select>
        </VStack>
      );
    };

export default SortByArea