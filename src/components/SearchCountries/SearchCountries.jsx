import React, {useState} from 'react'
// import './SearchCountries.css'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { Input, Flex, InputGroup, InputLeftElement } from '@chakra-ui/react';
import { SearchIcon } from '@chakra-ui/icons';

const SearchCountries = ({ allCountries, updateSearchCountries, updateSearchText }) => {
  const [inputValue, setInputValue] = useState('')

  const handleInputChange = (event) => {
    const { value } = event.target
    setInputValue(value)

    const countriesFound = findCountries(value)


    if (!countriesFound.length) {
      updateSearchCountries("No Such countries found!")
      updateSearchText(value)

    } else {

    updateSearchCountries(countriesFound)
    updateSearchText(value)
    }
  }


  const findCountries = (value) => {
    return allCountries.filter((country) =>
    country.name.common.toLowerCase().includes(value.toLowerCase()))
  }


return(
  <Flex >
    <InputGroup>
    <InputLeftElement children={<SearchIcon color="#9a9996"/>} />
    <Input
      type="text"
      value={inputValue}
      onChange={handleInputChange}
      placeholder="Search for a country..."
      color='#bbbbbb'
    >
    </Input>
    </InputGroup>
  </Flex>
)
}


export default SearchCountries
