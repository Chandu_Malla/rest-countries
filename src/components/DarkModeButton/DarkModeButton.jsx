import React, { useContext } from 'react';
import { MoonIcon } from '@chakra-ui/icons';
import { useHeaderContext } from '../../context/HeaderContext';
import { Button } from "@chakra-ui/react";


const DarkModeButton = ({backgroundStyle}) => {
    const { darkModeValue, setDarkModeValue } = useHeaderContext();

    // const {colorMode, toggleColorMode} = useColorMode()

    const changeDarkModeValue = () => {
        setDarkModeValue(!darkModeValue)
    }

    return (
        <Button
          variant="ghost"
          colorScheme="gray"
          size="md"
          p="1rem"
          onClick={changeDarkModeValue}
          leftIcon={<MoonIcon />}
          style={{ color: 'grey', fontSize: '14px' }}
        >
          Dark Mode
        </Button>
      );
}


export default DarkModeButton