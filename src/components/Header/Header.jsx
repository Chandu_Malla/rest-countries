import CountryDetail from "../CountryDetail/CountryDetail"
import DarkModeButton from "../DarkModeButton/DarkModeButton"
import { useHeaderContext } from "../../context/HeaderContext"

const Header = () => {
    const {darkModeValue, setDarkModeValue} = useHeaderContext()

    return(<>
    <header id="headerDiv" style={{ backgroundColor: !darkModeValue ? 'white' : 'hsl(209, 23%, 22%)' ,
                                    border: '1px soloid grey'
                                    }}>
    <h1 style={{ color: darkModeValue ? 'white' : 'black' }}>Where in the world?</h1>
    <div id="darkModeButton">
        <DarkModeButton />
    </div>
    </header>
    </>)
}


export default Header