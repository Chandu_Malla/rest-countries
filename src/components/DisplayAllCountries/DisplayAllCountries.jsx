import { useEffect, useState } from 'react'

 const DisplayAllCountries = ({ allCountries, updateDisplayCountries }) => {

  useEffect(() => {        
    updateDisplayCountries(allCountries);
  }, [])

  }

export default DisplayAllCountries;