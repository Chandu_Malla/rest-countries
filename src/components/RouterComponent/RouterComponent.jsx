import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import App from '../../App';
import CountryDetail from '../CountryDetail/CountryDetail';

const RouterComponent = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="/country/:id" element={<CountryDetail />} />
      </Routes>
    </Router>
  );
};

export default RouterComponent;
