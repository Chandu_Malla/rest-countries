import { useState } from "react"
// import './FilterByRegionOptions.css'
import { Select, VStack } from '@chakra-ui/react';


const FilterByRegionOptions = ( {allCountries, updateFilteredCountries, updateOption }) => {

  const [regionValue, setRegionValue] = useState(null)

    if (!allCountries ) {
      return null
    }
    
    const regionsSet = new Set()
    allCountries.forEach((country) => regionsSet.add(country.region))


    const handleRegionChange = (selectedRegion) => {
      // const selectedRegion = event.target.value;
      setRegionValue(selectedRegion)


      const filteredCountries = filterCountries(selectedRegion)
      updateFilteredCountries(filteredCountries)
      updateOption(selectedRegion)
    }
    
    const filterCountries = (region) => {
      if(!region) {
        return allCountries
      } 
      return allCountries.filter((country) => country.region === region)
    }

    return (
      <VStack spacing={4}>
          <Select placeholder="Filter by Region" onChange={(e) => handleRegionChange(e.target.value)}>
      {[...regionsSet].map((region, index) => (
        <option key={index} value={region}>
          {region}
        </option>
      ))}
    </Select>
      </VStack>
    );
  };
  
  export default FilterByRegionOptions
  