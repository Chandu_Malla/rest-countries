// import './FilterBySubRegion.css'
import { Select, VStack } from '@chakra-ui/react';
const FilterBySubRegion = ({ allCountries, subRegionsMap, optionValue, updateSubRegionData }) => {

    const searchSubRegions = (event) => {
        const selectedSubRegion = event.target.value
        const filteredCountries = allCountries.filter((country) => country.subregion === selectedSubRegion)
        updateSubRegionData(filteredCountries)
    }

    
    return (
        <VStack spacing={4}>
          <Select id="selectSubRegions" onChange={(e) => searchSubRegions(e)}>
            <option value="">Select SubRegion of {optionValue}</option>
            {Array.from(subRegionsMap.get(optionValue)).map((subRegion, index) => (
              <option key={index} value={subRegion}>
                {subRegion}
              </option>
            ))}
          </Select>
        </VStack>
      );
    };

export default FilterBySubRegion