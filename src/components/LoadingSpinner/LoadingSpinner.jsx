import React from 'react';
import { Box, Spinner } from '@chakra-ui/react';

const LoadingSpinner = () => {
  return (
    <Box display="flex" justifyContent="center" alignItems="center" h="100vh"
    minHeight='100vh' minWidth='100vw'>
      <Spinner size="xl" color="blue.500" />
    </Box>
  );
};

export default LoadingSpinner;
