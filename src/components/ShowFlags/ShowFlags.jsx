import { useEffect, useState } from "react"
import { useActionData, useNavigate } from "react-router-dom"
import { useHeaderContext } from "../../context/HeaderContext"
import { Card, CardBody, CardHeader, Flex, Text, Image, Box } from "@chakra-ui/react";


const ShowFlags = ({ countriesData }) => {

const navigate = useNavigate()
const [id, setId] = useState(null)

const {darkModeValue, setDarkModeValue} = useHeaderContext()

const flagsContainerStyles = {
  backgroundColor: !darkModeValue ? 'white' : 'hsl(209, 23%, 22%)'
}

const flagStyle = {
  color: darkModeValue ? 'white' : 'black'
}

const handleFlagDetails = (event, countryId) => {
  console.log('id', countryId);
  setId(countryId)
  navigate(`/country/${countryId}`)
}


const boxStyles = {
  h:'3vh',
  marginBottom:'0.5vh'
}



  if ( typeof countriesData === 'string') {
    return <p style={{ color: 'black', height: '100vh', textAlign: 'center' }}>No Such Countries Found!</p>;
  } else {

  return (<>
            
    <Flex gap="1rem" wrap='wrap' m="1rem" justifyContent={"space-around"}>
{countriesData.map((country,index) => (
  <Card key={index} w='20%' h='47vh' marginTop="1rem" marginBottom="1rem"
  style={flagsContainerStyles}
  onClick={(event) => handleFlagDetails(event, country.name.common)} > 
        <CardHeader p='0'>
       <Image 
        src={country.flags.png}
        alt={`Flag of ${country.name.common}`}
        w='100%'
        h='20vh'
        m="0"
       />
       </CardHeader>
       <CardBody marginTop="10px" paddingLeft="2rem">
       <Text as="h2" marginBottom="15px" fontSize="1.6rem" style={flagStyle}><strong>{country.name.common}</strong></Text>

       <Box style={boxStyles}>
        <Text as="span" style={flagStyle}><strong>Population:</strong></Text>
        <Text as="span" style={flagStyle}>{country.population}</Text>
      </Box>

      <Box style={boxStyles}>
        <Text as="span" style={flagStyle}><strong>Region:</strong></Text>
        <Text as="span" style={flagStyle}>{country.region}</Text>
        </Box>

        <Box style={boxStyles}>
        <Text as="span" style={flagStyle}><strong>Capital:</strong></Text>
        <Text as="span" style={flagStyle}>{country.capital}</Text>
        </Box>

       </CardBody>
</Card>
      ))}
</Flex> 
    </>
    )
  }
}

export default ShowFlags;


