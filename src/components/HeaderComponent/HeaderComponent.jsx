import CountryDetail from "../CountryDetail/CountryDetail"
import DarkModeButton from "../DarkModeButton/DarkModeButton"
import { useHeaderContext } from "../../context/HeaderContext"
import { Flex, Heading, Box } from '@chakra-ui/react'

const HeaderComponent = () => {
    const {darkModeValue, setDarkModeValue} = useHeaderContext()

    return(
        <Flex 
         bg={darkModeValue ? "hsl(209, 23%, 22%)" : "white"}
         border="1px solid grey"
         alignItems="center"
         p="1rem"
         justifyContent="space-between"
         boxSizing="border-box"
         paddingLeft='2rem'
        >
            <Heading 
            as="h1"
            color={darkModeValue ? "white" : "black"}
            fontSize="2xl"
            fontWeight="bold"
            >
            Where in the world?
            </Heading>
            <Box paddingRight='2rem'>
                <DarkModeButton />
            </Box>
        </Flex>
    )
}


export default HeaderComponent