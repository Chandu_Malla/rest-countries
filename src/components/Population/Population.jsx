import { Select, VStack } from '@chakra-ui/react';

const Population = ({allCountries, updatePopulationData}) => {
    const countryPopulation = allCountries.slice()

    const sortAscending = () => {
        countryPopulation.sort((countryA, countryB) =>  {
            return countryA.population - countryB.population
        })

        updatePopulationData(countryPopulation)
    }


    const sortDescending = () => {
        countryPopulation.sort((countryA, countryB) =>  {
            return countryB.population - countryA.population
        })

        updatePopulationData(countryPopulation)
    }



    const handlePopulation = (selectePopulationOrder) =>  {
        // const selectePopulationOrder = event.target.value

        if (selectePopulationOrder === "Ascending"){
            sortAscending()
        } else {
            sortDescending()
        }

    }

    return (
        <VStack spacing={4}>
          <Select id="selectPopulation" onChange={(e) => handlePopulation(e.target.value)}>
            <option value="">Choose Population Order</option>
            <option value="Ascending">Ascending Order</option>
            <option value="Descending">Descending Order</option>
          </Select>
        </VStack>
      );
    };

export default Population