import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import HeaderComponent from "../HeaderComponent/HeaderComponent";
import LoadingSpinner from "../LoadingSpinner/LoadingSpinner";
import { useHeaderContext } from "../../context/HeaderContext";
import { Flex, Box, Image, Button, Text } from "@chakra-ui/react";
import { ArrowBackIcon } from '@chakra-ui/icons';

const CountryDetail = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const API = `https://restcountries.com/v3.1/name/${id}`;
  const [countryDetails, setCountryDetails] = useState(null);
  const [loading, setLoading] = useState(true);

  const {darkModeValue, setDarkModeValue} = useHeaderContext()

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(API);
        if (!response.ok) {
          throw new Error('Failed to fetch country details');
        }
        const jsonData = await response.json();
        setCountryDetails(jsonData);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching data:', error);
        setLoading(false);
      }
    };

    fetchData();
  }, []);


  const flagsContainerStyles = {
   backgroundColor: !darkModeValue ? 'white' : 'black'
   }

   const flagStyle = {
   color: darkModeValue ? 'white' : 'black',
   }


  const handleBack = () => {
    navigate(`/`);
  };

  if (loading) {
    return <LoadingSpinner />;
  }

  if (!countryDetails) {
    return <div>Error: Failed to fetch country details</div>;
  }

  const extractedData = countryDetails.map(country => ({
    commonName: country.name.common,
    nativeName: country.name.nativeName[Object.keys(country.name.nativeName)[0]].common,
    population: country.population,
    region: country.region,
    subregion: country.subregion,
    capital: Array.isArray(country.capital) ? country.capital[0] : country.capital,
    topLevelDomain: Array.isArray(country.tld) ? country.tld[0] : country.tld,
    currencies: Object.keys(country.currencies)[0],  
    languages: (Object.values(country.languages)).length === 1
      ? Object.values(country.languages)[0]
      : Object.values(country.languages).map(language => language.name).join(','),
    borderCountries: country.borders,
    flagUrl: country.flags.png,
    flagPopulation: country.population 
  }));

  return (
    <>
      <HeaderComponent />
      <Flex h="100vh" w="100vw" 
      flexDirection="column" border="1px solid blue"
      boxSizing="border-box" p="2rem"
      style={flagsContainerStyles}>

        <Box w="20%" marginBottom='10vh' >
          <Button
           w='40%'
           textAlign='center'
            backgroundColor="white"
            color="black"
            border="1px solid grey"
            onClick={handleBack}
            leftIcon={<ArrowBackIcon />}
          >
            Back
          </Button>
        </Box>
        {extractedData.map((country, index) => (
          <Flex key={index} flexDirection='column' >

           <Flex >

            <Box borderWidth="2px"  w="40%" h="80%">
              <Image w="100%" h="100%" src={country.flagUrl} alt={`Flag of ${country.commonName}`} />
            </Box>

            {/* <Flex > */}

              <Flex w='60%' h='80%' justifyContent='space-around' alignItems='center' flexDirection='column' >
               
              <Flex gap='1rem' flexDirection='row' w='auto' h='80%'
              // boxSize='borderbox' 
              // p='2rem'
              >

                <Flex gap='1rem' flexDirection='column'  alignItems='start' h='80%' mr='3rem'>
               {/* <Box> */}
                <Text as="h2" style={flagStyle} fontSize='1.5rem'><strong>{country.commonName}</strong></Text>
                  <Text style={flagStyle} ><strong>NativeName:</strong><Text as="span" style={flagStyle}>{country.nativeName}</Text></Text>
                  <Text style={flagStyle}><strong>Population:</strong> <Text as="span" style={flagStyle}>{country.population}</Text></Text>
                  <Text style={flagStyle}><strong>Region:</strong> <Text as="span" style={flagStyle}>{country.region}</Text></Text>
                  <Text style={flagStyle}><strong>Subregion:</strong> <Text as="span" style={flagStyle}>{country.subregion}</Text></Text>
                  <Text style={flagStyle}><strong>Capital:</strong> <Text as="span" style={flagStyle}>{country.capital}</Text></Text>
                {/* </Box> */}
                </Flex>

                <Flex gap='1rem' flexDirection='column' justifyContent='center' alignItems='center'  h='60%' ml='6rem'>
                {/* <Box> */}
                  <Text style={flagStyle}><strong>Top Level Domain:</strong> <Text as="span" style={flagStyle}>{country.topLevelDomain}</Text></Text>
                  <Text style={flagStyle}><strong>Currencies:</strong> <Text as="span" style={flagStyle}>{country.currencies}</Text></Text>
                  <Text style={flagStyle}><strong>Languages:</strong> <Text as="span" style={flagStyle}>{country.languages}</Text></Text>
                {/* </Flex> */}
                </Flex>

                </Flex> 

              <Box ml='-5rem'>
              <Text style={flagStyle}><strong>Border Countries: </strong>{country.borderCountries ? (
                  <Text as="span" style={flagStyle}>
                     {country.borderCountries.map((borderCountry, index) => (
                     <Text as='span' style={flagStyle} p='0.5rem' marginRight='0.5rem' key={index} maxW='rem' style={{ border: '0.5px solid grey' }}>{borderCountry}</Text>
                     ))}
                  </Text>
               ) : <Text as="span" style={flagStyle}>None</Text>}</Text>
               </Box>

              </Flex>

            </Flex>
              
            

            {/* </Flex> */}

          </Flex>
        ))}
      </Flex>
    </>
  );
};

export default CountryDetail;
